let contests = [
  [5,1], [2,1], [1,1], [8,1], [10,0], [5,0]
]

let contests2 = [
  [90,1],[77,0],[68,0],[57,0],[83,1],[49,0],[73,0],[63,0],[61,0],[66,1]
]

luckBalance = (k,contests) => {
  let sorted = contests
    .filter((val, i, arr) => arr[i][1] == 1)
    .map(i => i[0])
    .sort((a,b) => b-a)
  
  let lose = sorted.splice(0,k).reduce((a,b) => a+b, 0)

  let win = sorted.splice(0,contests.length).reduce((a,b) => a+b, 0)

  let notImportant = contests.filter((val, i, arr) => arr[i][1] == 0)
                    .map(i => i[0])
                    .reduce((a,b) => a+b, 0)

  let output = lose - win + notImportant
  return output
}

let result = luckBalance(1,contests2)
console.log(result)