// This code is based on youtube
// https://www.youtube.com/watch?v=Otf9lp3DQJo
// And for the stack implementation is from
// https://medium.com/better-programming/implementing-a-stack-in-javascript-73d1aa0483c1


const s = '{[][()]}'

class Stack {
  constructor(){
    this.data = []
    this.top = 0
  }
  push(element) {
    this.data[this.top] = element
    this.top = this.top + 1
  }
  length() {
    return this.top;
  }
  isEmpty() {
    return this.top === 0;
  }
  pop() {
    if( this.isEmpty() === false ) {
       this.top = this.top -1
       return this.data.pop() // removes the last element
     }
  }
}

isBalanced = s => {
  let stack = new Stack()
  for (let index = 0; index < s.length; index++) {
    if(s.charAt(index) === '(' 
    || s.charAt(index) === '{' 
    || s.charAt(index) === '[')
      stack.push(s.charAt(index))
    else {
      if(stack.isEmpty())
        return 'NO'
      else {
        let popVal = stack.pop()
        if(s.charAt(index) === ')' && popVal !== '(')
          return 'NO'
        else if(s.charAt(index) === '}' && popVal !== '{')
          return 'NO'
        else if(s.charAt(index) === ']' && popVal !== '[')
          return 'NO'
      }
    }
  }
  if(stack.isEmpty())
    return 'YES'
  else
    return 'NO'
}
let result = isBalanced(s)
console.log(result)