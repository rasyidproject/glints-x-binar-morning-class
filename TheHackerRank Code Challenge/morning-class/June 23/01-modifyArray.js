const arr = [1,2,3,4,5]

modifiedArray = num => {
  let result = []

  num.forEach(i => {
    if((i%2) == 0) result.push(i*2)
    else result.push(i*3)
  })
  return result
}