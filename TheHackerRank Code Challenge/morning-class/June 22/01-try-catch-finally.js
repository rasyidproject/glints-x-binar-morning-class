let word = 12345

reverseString = s => {
  try {
    if(typeof s !== 'string'){
      throw new Error('s.split is not a function')
    }
    let reverse = s.split('').reverse()
    let result = reverse.join('')
    console.log(result)
  } catch (error) {
    console.log(error.message)
    console.log(s)
  }
}

reverseString(word)