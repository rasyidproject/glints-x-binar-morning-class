const Node = class {
  constructor(nodeData) {
      this.data = nodeData;
      this.next = null;
      this.prev = null;
  }
}

const DoublyLinkedList = class {
  constructor() {
      this.head = null;
      this.tail = null;
  }

  insertNode(nodeData) {
      let node = new Node(nodeData);

      if (!this.head) {
          this.head = node
      } else {
          this.tail.next = node;
          node.prev = this.tail;
      }

      this.tail = node;
  }
}

function printDoublyLinkedList(node) {
  let x = node
  while(x){
    console.log(x.data)
    x = x.next
  }
}

sortedInsert = (head, data) => {
  let node = new Node(data)
  node.data = data
  if(head == null) {
    return node
  }
  else if (data < head.data){
    node.next = head
    head.prev = node
    return node
  }else {
    let rest = sortedInsert(head.next, data)
    head.next = rest
    rest.prev = head
    return head
  }
}

let list = new DoublyLinkedList()
list.insertNode(1)
list.insertNode(3)
list.insertNode(4)
list.insertNode(10)

let sortedList = sortedInsert(list.head, 5)

printDoublyLinkedList(sortedList)