alternatingCharacters = s => {
  let word = s
  let split = word.split('')
  let count = 0
  for (let index = 0; index < split.length-1; index++) {
    if(split[index] == split[index+1]){
      count++
    }
  }
  return count
}

console.log(alternatingCharacters('ABABABABABABABABAAABA'))