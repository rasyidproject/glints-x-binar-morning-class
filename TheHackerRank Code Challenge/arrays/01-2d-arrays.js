const arr = [
  [1,1,1,0,0,0],
  [0,1,0,0,0,0],
  [1,1,1,0,0,0],
  [0,0,0,0,0,0],
  [0,0,0,0,0,0],
  [0,0,0,0,0,0]
]

hourglassSum = (arr) => {
  let arrSum = []
  arr.forEach((valueRow,indexRow) => {
    arr[indexRow].forEach((valueColumn,indexColumn) => {
      console.log(`this is index [${indexRow}][${indexColumn}] and the value is ${valueColumn}`)
      if(indexRow < 4 && indexColumn < 4) {
        console.log('calculate here')
        let sum = 
          arr[indexRow][indexColumn] + arr[indexRow][indexColumn+1] + arr[indexRow][indexColumn+2]
        + arr[indexRow+1][indexColumn+1] 
        + arr[indexRow+2][indexColumn] + arr[indexRow+2][indexColumn+1] + arr[indexRow+2][indexColumn+2]
        console.log(sum)
        arrSum.push(sum)
      }
    })
  })
  let largest = Math.max(...arrSum)
  console.log(largest)
  return largest
}