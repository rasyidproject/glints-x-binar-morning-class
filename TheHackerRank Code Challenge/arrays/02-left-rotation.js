const arr = [1,2,3,4,5]

rotLeft = (a,d) => {
  console.log(a)
  let left = []
  a.forEach((value, index) => {
    if(index+d < a.length){
      left[index] = a[index+d]
    } else {
      let offset = (index+d) % a.length
      left[index] = a[offset]
    }
  })
  return left
}

const result = rotLeft(arr,3)
console.log(result)