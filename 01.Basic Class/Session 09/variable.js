//Variable Declaration
let name = "Rasyid";

var address = "Yogyakarta";

// immutable variable of an alias, UPPERCASE_CONSTANT
const IS_MARRIED = false;
const RED = "FF0000";

// cammelCase for mutable variable
let isPasswordValid;

// We can use $ and _ to name a variable
const $TEST = "This is a variable with $ sign";

const _SECRET = "This is secret";

// We can declare a variable without assigning value;
let a;
console.log(a);
a = 10;
console.log(a);

// Variable can be assign with data types, function dan class

let person = {
    name: "Rasyid",
    address: "Yogyakarta",
    married: false,
    born: 1990
}

console.log(person);

// Function must have a return value, while procedure doesn't return a value.

// There are 3 ways to create a function

function firstFunction(){
    // Do something here
}

const SECOND_FUNCTION = function(){
    // Do something here
}

const ANOTHER_FUNCTION_WITH_ARROW = x => {
    console.log(`You just ${x}`);
}

const MORE_ARROW = (x, y, z) => {
    console.log(`x = ${x}, y = ${y}, z = ${z}`);
}

ANOTHER_FUNCTION_WITH_ARROW(7);
MORE_ARROW(1,2,3);

// Arrow Function vs Function keywords

function keywords(){
    console.log(this);
}

let arrow = (a) => {
    console.log(`Output is ${this}`);
}

keywords();
arrow("Rasyid");

function example(x){
    if(x >= 10){
        return x / 2;
    } else {
        throw new Error('Input should have been more than 10');
    }
}

try {
    let result = example(9);
    console.log(result);
} catch (error) {
    console.log(error.message);
}