## What I learn in this course.

### If else statement

```javascript
let i = true;
if(i){
    console.log("That is true");
}
```
In that code, output will be shown because condition is true.
```javascript
let i = false;
if(i){
    console.log("That is not right");
}else{
    console.log("That's right");
}
```
In this code, the output is "That's right"

----

### Variable

We can use <code>let</code> or <code>var</code> to declare a variable in javascript.

```javascript
let name = "Rasyid";
var address = "Yogyakarta";
```

Variable can be assign with data types, function dan class

```javascript
let person = {
    name: "Rasyid",
    address: "Yogyakarta",
    married: false,
    born: 1990
}
```
There are different variable naming style. 
Mutable variable use camelCase style, while for immutable variable we can use all-uppercase style
```javascript
let sport = "Running";
const EARTH_IS_FLAT = false; 
```
We can name a variable start with <code>$</code> and <code>_</code> sign. 
```javascript
const $TEST="You shall not pass";
const _SECRET = "I'm Batman"; 
```

----

### Function and the type of it
```javascript
function commonFunction(){
    console.log("OK Boomer");
}

let functionAssignedToVariable = function(){
    console.log("Millenials rule the world");
}

let arrowFunction = x => {
    console.log(`Hey ${x}, you are awesome`);
}
```

----

### Try catch statement and throw message

```javascript
function example(x){
    if(x >= 10){
        return x / 2;
    } else {
        throw new Error('Input should have been more than 10');
    }
}

try {
    let result = example(9);
    console.log(result);
} catch (error) {
    console.log(error.message);
}
```

<script src=https://gitlab.com/rasyidproject/glints-x-binar-morning-class/-/blob/master/01-basic-class/session-09/variable.js"></script>