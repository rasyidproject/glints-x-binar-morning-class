const Tube = require(`./Tube`)

function inputOption(){
    console.log(`What do you want to calculate? 
    1. Cylinder 
    `);
    
    Tube.createInput.question(`Answer : `, answer => {
        handleInput(answer)
    })
}

inputOption()

function handleInput(answer){
    console.log(`answer on handleInput ${answer}`)
    switch (Number(answer)) {
        case 1:
            Tube.inputCylinderOption()
            break;
    
        default:
            console.clear()
            console.log(`Option not available. Choose Again
            `)
            inputOption()
            break;
    }
}