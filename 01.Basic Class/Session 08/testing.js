const readline = require("readline");

const rl = readline.createInterface({
    input : process.stdin,
    output : process.stdout
});

function inputOption(){
    console.log(`What do you want to solve? 
    1. Cylinder Volume
    2. Cylinder Surface Area
    `);
    rl.question("Answer : ", a => {
        handleAnswer(a);
    })
}

inputOption();

function handleAnswer(a){
    console.clear();
    switch(Number(a)){
        case 1: console.log("Option 1");
            rl.close();
            break;
        case 2: console.log("Option 2");
            rl.close();
            break;

        default:
            console.log(`Option is not available. Choose again
            `);
            inputOption();
            break;
    }
}