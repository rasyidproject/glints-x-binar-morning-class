class Human {

    static properties = [
        `name`,`language`
    ]
    
    constructor(props){
        this._validate(props)
        this.name = props.name
        this.language = props.language
    }

    _validate(props) {
        if (typeof props !== 'object' || Array.isArray(props))
          throw new Error("Constructor needs object to work with");
    
        this.constructor.properties.forEach(i => {
          if (!Object.keys(props).includes(i))
            throw new Error(`${i} is required`);
        });
    }

    speakWith(Human){
        let check = 0
        for (let index = 0; index < Object.values(Human.language).length; index++) {
            if(this.language.includes(Human.language[index])){
                check = 1
                console.log(`${this.name} and ${Human.name} can speak with each other in ${Human.language[index]}`)
            } 
        }
        if(check == 0)
            console.log(`${this.name} can't speak with ${Human.name}`)
        return check;
    }
    marry(Human){
        console.log(`${this.name} will marry with ${Human.name}`)
        if(this.speakWith(Human) == 0){
            console.log(`Before married, someone need to learn a new language`)

            // Make a random number to decide who will learn new language
            let who = Math.round(Math.random())

            // Create variable to store a random number to decide which language need to learn
            let lang;

            // If value = 0, this human will learn a new language and vice versa 
            // if value = 1
            if(who == 0){
                lang = Math.floor(Math.random() * Object.values(Human.language).length)
                this.learnLanguage(Human.language[lang])
            }else if(who == 1){
                lang = Math.floor(Math.random() * Object.values(this.language).length)
                Human.learnLanguage(this.language[lang])
            }
        }
        console.log(`${this.name} now married with ${Human.name}`)
    }

    gretting(Human){
        console.log(`Hi ${Human.name}. How do you do?`)
    }

    introduce(){
        console.log(`Hello, my name is ${this.name}`)
    }

    learnLanguage(language){
        if(this.language.includes(language)){
            console.log(`${this.name} already learn ${language}`)
        }else{
            this.language.push(language)
        console.log(`${this.name} now learn ${language}`)
        }
    }

    cook(){
        console.log(`${this.name} can cook`)
    }
}

module.exports = Human