const jwt = require(`jsonwebtoken`)
const bcrypt = require(`bcryptjs`)

require('dotenv').config()

'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: true,
        notNull: true
      }
    },
    encrypted_password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    verified: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    }
  }, {
    underscored: true,
  });
  User.associate = function(models) {
    // associations can be defined here
  };

  // Public Prototype Getter
  Object.defineProperty(User.prototype, 'entity', {
    get() {
      return {
        id: this.id,
        email: this.email,
        verified: this.verified,
        access_token : this.getToken()
      }
    }
  })

  // Public Static Authenticate Method
  User.authenticate = async function({email,password}){
    console.log(`${email} : ${password}`)
    try {
      let instance = await this.findOne({
        where : { email: email.toLowerCase() }
      })
      if (instance == null) return Promise.reject(new Error(`Email doesn't exist`))

      let isValidPassword = instance.checkCredential(password)
      if(!isValidPassword) return Promise.reject(new Error("Wrong password"))

      return Promise.resolve(instance)
    } catch (err) {
      return Promise.reject(new Error(`Error`))
    }
  }

  // Public checkCredential Method
  User.prototype.checkCredential = function(password) {
    return bcrypt.compareSync(password, this.encrypted_password)
  }

  // Public getToken Method
  User.prototype.getToken = function() {
    return jwt.sign({
      id: this.id,
      email: this.email,
      verified : this.verified
    }, process.env.TOKEN)
  }

  // Hooks
  User.beforeCreate(function(user) {
    const hash = bcrypt.hashSync(user.encrypted_password,10)
    user.encrypted_password = hash
    user.email = (user.email).toLowerCase()
  })
  return User;
};