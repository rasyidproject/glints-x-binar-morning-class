const { User } = require('../models')

const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

module.exports = {

  register: async (req, res, next) => {
    req.body.email = req.body.email || ""
    let getEmail = (req.body.email).toLowerCase()
    try {
      let user = await User.create({
        email: getEmail,
        encrypted_password: req.body.password
      })
      res.status(201)
      req.data = user.entity
      next()
    } catch (error) {
      next(error)
    }
  },

  getUser: async (req, res, next) => {
    try {
      let data = await User.findAll()
      res.status(200)
      req.data = data
      next()
    } catch (error) {
      res.status(422)
      next(error)
    }
  },

  login: async (req, res, next) => {
    try {
      let user = await User.authenticate(req.body)
      res.status(201)
      req.data = user.entity
      next()
    } catch (error) {
      res.status(401)
      next(error)
    }
  },
  
  me: async(req, res, next) => {
    res.status(200)
    req.data = req.user.entity
    next()
  },

  update: async(req, res, next) => {
    // If password is exist in body, hash first before updating to database
    if(req.body.password != null)
      req.body.encrypted_password = bcrypt.hashSync((req.body.password).toString(),10)

    try {
      let data = await User.update(
        req.body, {
          where: {
            id: req.params.id
          }
      })
      // If data not found, send error message
      if(data[0] === 0){
        res.status(404)
        return next(new Error(`There are no data with id ${req.params.id}`))
      }

      res.status(200)
      req.data = `data from id ${req.params.id} is updated`
      next()
    } catch (error) {
      res.status(404)
      next(error)
    }
  },

  delete: async(req, res, next) => {
    try {
      let data = await User.destroy({
        where: {
          id: req.params.id
        }
      })
      // If data not found, send error message
      if(data === 0){
        res.status(404)
        return next(new Error(`There are no data with id ${req.params.id}`))
      }

      res.status(204)
      req.data = `data from id ${req.params.id} is deleted`
      next()
    } catch (error) {
      res.status(404)
      next(error)
    }
  }
}