const { Product } = require('../models');
const { Op } = require('sequelize')

module.exports = {
  all: async(req, res, next) => {
    try {
      let data = await Product.findAll()
      res.status(200)
      req.data = data
      next()
    } catch (error) {
      res.status(404)
      next()
    }
  },

  find: async(req, res, next) => {
    try {
      let data = await Product.findOne({
        where: {
          id: req.params.find
        }
      })
      // If data is not found, return new error
      if(data == null){
        res.status(404)
        return next(new Error (`Data not found`))
      } 

      res.status(200)
      req.data = data
      next()
    } catch (error) {
      next(error)
    }
  },

  available: async(req, res, next) => {
    try {
      let data = await Product.findAll({
        where: {
          stock:{
            [Op.gt]:0 //It means stock>0
          }
        }
      })
      res.status(200)
      req.data = data
      next()
    } catch (error) {
      next(error)
    }
  },

  insert: async(req, res, next) => {
    try {
      let data = await Product.create(req.body)
      res.status(201)
      req.data = data
      next()
    } catch (error) {
      next(error)
    }
  },

  update: async(req, res, next) => {
    try {
      let data = await Product.update(
        req.body, {
          where: {
            id: req.params.id
          }
      })
      // If data not found, send error message
      if(data[0] === 0){
        res.status(404)
        return next(new Error(`There are no data with id ${req.params.id}`))
      }
      
      res.status(200)
      req.data = `data from id ${req.params.id} is updated`
      next()
    } catch (error) {
      next(error)
    }
  },

  delete: async(req, res, next) => {
    try {
      let data = await Product.destroy({
        where: {
          id: req.params.id
        }
      })
      // If data not found, send error message
      if(data === 0){
        res.status(404)
        return next(new Error(`There are no data with id ${req.params.id}`))
      }

      res.status(204)
      req.data = `data from id ${req.params.id} is deleted`
      next()
    } catch (error) {
      res.status(404)
      next(error)
    }
  }
}