const jwt = require('jsonwebtoken')
const {User} = require('../models')

module.exports = async (req, res, next) => {
  let token = req.headers.authorization
  try {
    let payload = await jwt.verify(token, process.env.TOKEN)
    req.user = await User.findByPk(payload.id)
    next()
  }
  catch(err) {
    next(new Error(`Invalid Token !!`))
  }
}