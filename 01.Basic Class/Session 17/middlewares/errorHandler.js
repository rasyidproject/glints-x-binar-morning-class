module.exports = [
    (req, res, next) => {
        res.status(404)
        next(new Error('Not found'))
    },

    (err, req, res, next) => {
        console.log(res.statusCode)
        if (res.statusCode == 200) res.status(500)
        res.status(res.statusCode).json({
          status: `fail`,
          errors: [err.message]
        })
    }
]