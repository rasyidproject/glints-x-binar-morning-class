const express = require(`express`)
const router = express.Router()
const product = require(`./controllers/productController`)
const user = require(`./controllers/userController`)
const authorizer = require(`./middlewares/authorizer`)
const presentHandler = require(`./middlewares/presentHandler`)

router.get(`/products`,product.all, presentHandler)
router.get(`/products/available`,product.available, presentHandler)
router.get(`/products/:find`,product.find, presentHandler)
router.post(`/products/insert`, authorizer, product.insert, presentHandler)
router.put(`/products/:id`, authorizer, product.update, presentHandler)
router.delete(`/products/:id`, authorizer, product.delete, presentHandler)

router.post(`/users/register`, user.register, presentHandler)
router.post(`/users/login`,user.login, presentHandler)
router.get(`/users`, authorizer, user.getUser, presentHandler)
router.get(`/users/me`, authorizer, user.me, presentHandler)
router.put(`/users/:id`, authorizer, user.update, presentHandler)
router.delete(`/users/:id`, authorizer, user.delete, presentHandler)

module.exports = router