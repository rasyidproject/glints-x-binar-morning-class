const express = require('express');
const app = express()
const morgan = require('morgan')

// Middleware
const router = require('./router.js');
const errorHandler = require(`./middlewares/errorHandler`)


// Middleware to Parse JSON
app.use(morgan(`tiny`))
app.use(express.json()); // Coba sendiri.

app.get('/', function (req, res) {
  res.json({
    status: true,
    message: "Hello Mars!! I'm coming"
  })
})

app.use('/', router);

errorHandler.forEach(handler => app.use(handler))
 
app.listen(3000, () => {
  console.log("Listening on port 3000!");
})
