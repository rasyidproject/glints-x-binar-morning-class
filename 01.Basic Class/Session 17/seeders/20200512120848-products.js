'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */

    return queryInterface.bulkInsert('products',[
      // {
      // name: "nike air zoom alphafly next%",
      // price: 2700000,
      // stock: 10,
      // createdAt: new Date(),
      // updatedAt: new Date()
      // },
      // {
      //   name: "adidas ultraboost 20",
      //   price: 3000000,
      //   stock: 5,
      //   created_at: new Date(),
      //   updated_at: new Date() 
      // },
      {
        name: "asics glideride",
        price: 2299000,
        stock: 0,
        created_at: new Date(),
        updated_at: new Date() 
      }
    ])
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
