const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

function greet(name, address, birthday){
    console.log(`Hello ${name}, looks like you're ${new Date().getFullYear()-birthday}! And you lived in  ${address}`);
}

console.log("Government Registry\n");
rl.question("What is your name? ", name => {
    rl.question("Which city do you live? ", address => {
        rl.question("When was your birthday year? ", birthday => {
            greet(name,address,birthday);
            rl.close();
        })
    })
})

rl.on("close", () => {
    process.exit();
})