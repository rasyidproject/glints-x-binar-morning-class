# Session 16 Note

### Link to previous code
[Sequelize code](../Session 15/Sequelize)

### Today I modify previous code with better HTTP status code. 

| Message | Description |
| ---     | --------    |
| 200     | The request is OK. |
| 201     | The request is complete, and a new resource is created. |
| 401 | The requested page needs a username and a password.
| 422 | Unprocessable Entity


### I learn there are alot of HTTP request. In this session, we use <code>get</code>, <code>put</code> and <code>delete</code>.

```javascript
router.get(`/products`,product.all)
router.get(`/products/available`,product.available)
router.get(`/products/:find`,product.find)
router.post(`/products/insert`, product.insert)
router.put(`/products/:id`, product.update)
router.delete(`/products/:id`, product.delete)

router.post(`/users/register`, user.register)
router.post(`/users/login`,user.login)
router.get(`/users`, user.getUser)
```

### Today task is to add validation to user model.

Check email format. It the format is not correct, it will send an error
![Output if email format is not correct](output/session-16-01.jpg)

Check before create a new user to database. If user with the same email is already exist, it will send an error
![Output if the same email is already exist in database](output/session-16-02.jpg)