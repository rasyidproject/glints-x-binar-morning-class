'use strict';
const bcrypt = require(`bcryptjs`)
const jwt = require(`jsonwebtoken`)

module.exports = (sequelize, DataTypes) => {
  const users = sequelize.define('users', {
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: true,
        isLowercase: true,
        notEmpty: true
      }
    },
    encrypted_password: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    }
  }, {
    hooks: {
      beforeValidate: instance => {
        instance.email = instance ? instance.email.toLowerCase() : ""
      },
      beforeCreate: instance => {
        instance.encrypted_password = bcrypt.hashSync(instance.encrypted_password, 10)
      }
    }
  });

  users.associate = function(models) {
    // associations can be defined here
    users.hasMany(models.post, {
      foreignKey: `user_id`
    })
  };

  Object.defineProperty(users.prototype, 'entity', {
    get() {
      return {
        // id: this.id,
        // email: this.email
        id: this.id,
        email: this.email,
        access_token : this.getToken()
      }
    }
  })

  users.authenticate = async function({email,password}){
    try {
      let instance = await this.findOne({
        where : { email: email.toLowerCase() }
      })
      if (instance == null) return Promise.reject(new Error(`Email doesn't exist`))

      let isValidPassword = instance.checkCredential(password)
      if(!isValidPassword) return Promise.reject(new Error("Wrong password"))

      return Promise.resolve(instance)
    } catch (err) {
      return Promise.reject(new Error(`Error`))
    }
  }

  users.prototype.checkCredential = function(password) {
    return bcrypt.compareSync(password, this.encrypted_password)
  }

  users.prototype.getToken = function() {
    return jwt.sign({
      id: this.id,
      email: this.email
    }, process.env.TOKEN)
  }

  return users;
};