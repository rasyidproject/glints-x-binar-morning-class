const { post, users } = require('../models')

module.exports = {
    newPost: async (req, res, next) => {
        try {
            const data = await post.create({
                title: req.body.title,
                body: req.body.body,
                user_id: req.user.id
            })
            res.status(200)
            res.data = data
            next()
        } catch (error) {
            next(error)
        }
    },
    all: async (req, res, next) => {
        try {
            const data = await post.findAll({
                include: [users]
            })
            res.status(200)
            res.data = data
            next()
        } catch (error) {
            next(error)
        }
    },
    update: async (req, res, next) => {
        try {
            const data = await post.update(
                req.body, {
                    where: {
                        id: req.params.id
                    }
                }
            )

            // If data not found, send error message
            if(data[0] === 0){
                res.status(404)
                return next(new Error(`There are no post with id ${req.params.id}`))
            }

            res.status(200)
            res.data = `post from id ${req.params.id} is updated`
            next()
        } catch (error) {
            res.status(422)
            next(error)
        }
    },
    delete: async (req, res, next) => {
        try {
            const data = await post.destroy({
                where : {
                    id : req.params.id
                }
            })

            // If data not found, send error message
            if(data === 0){
                res.status(404)
                return next(new Error(`There are no data with id ${req.params.id}`))
            }

            res.status(204)
            req.data = `data from id ${req.params.id} is deleted`
            next()
        } catch (error) {
            res.status(404)
            next(error)
        }
    }
}