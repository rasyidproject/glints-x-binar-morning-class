const { users } = require(`../models`)
const bcrypt = require('bcryptjs')

module.exports = {
    register: async (req, res, next) => {
        try {
            const data = await users.create({
                email: req.body.email,
                encrypted_password : req.body.password
            })
            res.data = data
            next()
        } catch (error) {
            res.status(422)
            next(error)
        }
    },

    login: async (req, res, next) => {
        try {
            const data = await users.authenticate(req.body)
            res.status(201)
            res.data = data.entity
            next()
        } catch (error) {
            res.status(401)
            next(error)
        }
    },

    all: async (req, res, next) => {
        try {
            const data =  await users.findAll()
            res.status(200)
            res.data = data
            next()
        } catch (error) {
            res.status(404)
            next()
        }
    },

    update: async (req, res, next) => {
        try {
            // If password is exist in body, hash first before updating to database
            if(req.body.password != null)
            req.body.encrypted_password = bcrypt.hashSync((req.body.password).toString(),10)
            const data =  await users.update(
                req.body, {
                    where: {
                        id: req.params.id
                    }
                }
            )
            // If data not found, send error message
            if(data[0] === 0){
                res.status(404)
                return next(new Error(`There are no data with id ${req.params.id}`))
            }
            
            res.status(200)
            res.data = `data from id ${req.params.id} is updated`
            next()
        } catch (error) {
            next(error)
        }
    },
    delete: async (req, res, next) => {
        try {
            const data = await users.destroy({
                where: {
                    id: req.params.id
                }
            })
            res.status(204)
            req.data = `data from id ${req.params.id} is deleted`
            next()
        } catch (error) {
            res.status(404)
            next(error)
        }
    }
}