const router = require(`express`).Router()

// Core Module
const userController = require(`./controllers/usersController`)
const postController = require(`./controllers/postController`)
const success = require(`./middlewares/success`)
const authorizer = require(`./middlewares/authorizer`)

// User Collection API
router.post(`/users/register`, userController.register, success)
router.post(`/users/login`, userController.login, success)
router.get(`/users`, userController.all, success)
router.put(`/users/:id`, authorizer, userController.update, success)
router.delete(`/users/:id`, authorizer, userController.delete, success)

router.post(`/post/new`,authorizer, postController.newPost, success)
router.get(`/post`,postController.all, success)
router.put(`/post/:id`, authorizer, postController.update, success)
router.delete(`/post/:id`, authorizer, postController.delete, success)

module.exports = router