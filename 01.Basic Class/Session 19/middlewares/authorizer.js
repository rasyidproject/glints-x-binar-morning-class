const jwt = require('jsonwebtoken')
const {users} = require('../models')
const {post} = require('../models')

module.exports = async (req, res, next) => {
  let token = req.headers.authorization
  try {
    console.log(`im here ${req.method}`)
    let payload = await jwt.verify(token, process.env.TOKEN)
    req.user = await users.findByPk(payload.id)

    // Token check
    // Check if request if from users end point
    if(req.path.includes(`users`)) {

      // If user id from token did not match with user id from parameter, return error
      if(req.user.id != req.params.id)
        next(new Error(`You can only change your user data`))

    // Check if request if from post update and delete end point
    }else if(req.path.includes(`post`) && (req.method != `POST`)){

      req.post = await post.findByPk(req.params.id)

      // If user id from token did not match with 
      // user id from post with id from parameter, return error
      if(req.user.id != req.post.user_id)
        next(new Error(`This is not your post`))
    }
    next()
  }
  catch(err) {
    next(new Error(`Invalid Token`))
  }
}