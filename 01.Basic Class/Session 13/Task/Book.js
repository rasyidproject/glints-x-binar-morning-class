const Record = require(`./Record`)

class Book extends Record {
    static properties = {
        title : {
            type : `string`,
            required : true
        },
        author : {
            type : `string`,
            required : true
        },
        price : {
            type : `number`,
            required : true
        },
        publisher : {
            type : `string`,
            required : true
        }
    }
}

let onepiece = new Book({
    title : `One Piece`,
    author : `Eiichiro Oda`,
    price : 38000,
    publisher : `Weekly Shonen Jump`
})

let nanatsu = new Book({
    title : `Nanatsu no Tanzai`,
    author : `Nakaba Suzuki`,
    price : 40000,
    publisher : `Kodansha USA`
})

//Book.find(2).delete()
Book.find(7).update(nanatsu)

module.exports = Book