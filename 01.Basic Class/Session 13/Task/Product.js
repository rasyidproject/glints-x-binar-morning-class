const Record = require(`./Record`)

class Product extends Record {
    static properties = {
        name : {
            type : `string`,
            required : true
        },
        price : {
            type : `number`,
            required : true
        },
        stock : {
            type : `number`,
            required : true
        }
    }
}

let shoes = new Product ({
    name : `Nike`,
    price : 2000000,
    stock : 10
})

console.log(shoes)
shoes.save()

module.exports = Product