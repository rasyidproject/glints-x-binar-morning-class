const Record = require('./record');

class User extends Record {
  static properties = {
    email: {
      type: 'string',
      required: true,
      unique: true
    },
    encrypted_password: {
      type: 'string',
      required: true
    }
  }

  constructor(props) {
    super(props);
    this.password = this.encrypted_password;
  }

  set password(password) {
    return this.encrypted_password = `pretend-that-this-is-an-encrypted-version-of-${password}`
  }
}

// let rasyid = new User({
//     email: `rasyid@rasyid.com`,
//     encrypted_password : `lalaland`
// })

// let pevita = new User({
//     email: `pevita@awesome.com`,
//     encrypted_password: `catlover`
// })

// let wulan = new User({
//     email: `wulan.guritno@bestmom.com`,
//     encrypted_password: `yogaismylife`
// })

// let nadiem = new User({
//     email: `nadiem@gojek.com`,
//     encrypted_password: `tutuppoinsetiaphari`
// })

// rasyid.save()
// pevita.save()
// wulan.save()
// nadiem.save()

module.exports = User