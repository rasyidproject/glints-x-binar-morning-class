const Book = require('../Task/Book');
const Product = require(`../Task/Product`)
const User = require(`../Task/User`)

console.log(Book);

function handler(req, res) {
  switch(req.url) {
    case '/':
      res.write("Hello World");
      break;

    case `/users` :
      res.write(JSON.stringify(User.all, replacer, 2))
      break

    case '/books':
      res.write(JSON.stringify(Book.all, null, 2));
      break;

    case `/products` :
      res.write(JSON.stringify(Product.all, null, 2))
      break
    
    case `/products/available` :
      res.write(JSON.stringify(Product.all.filter(i => i.stock !== 0), null, 2))
      break

    default:
      res.write("404 Not Found!");
  }

  res.end();
}

const http = require('http'); // Import HTTP module
const app = http.createServer(handler);



app.listen(3000, () => {
  console.log("Listening on port 3000");
})

function replacer(key,value)
{
    if (key=="encrypted_password") return undefined;
    else return value;
}
