'use strict';

const express = require('express')
const app = express()
const cors = require('cors')
const port = 3000;

const router = require('./router')

// Data
const area = require('./db/area.json')

// Middleware
app.use(cors()); // CORS to allow front-end use our server
app.use(express.json()); // JSON parser

// GET /
app.get('/', (req, res) =>
  res.json({
    status: true,
    message: "<h1>Hello World!</h1>"
  })
);

app.use('/', router);

app.listen(port, () => 
  console.log(`Example app listening at http://localhost:${port}`)
)