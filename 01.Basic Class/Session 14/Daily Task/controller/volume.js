const area = require('../db/volume.json')

module.exports = {

  postCubeVolume: (req, res) => {
    let getEdge = req.body.parameter.edge
    res.status(201).json({
        form: req.body.form,
        edge: getEdge,
        result: getEdge*getEdge*getEdge
    })
  },
  postTubeVolume: (req, res) => {
    let getRadius = req.body.parameter.radius
    let getHeight = req.body.parameter.height
    res.status(201).json({
        form: req.body.form,
        radius: getRadius,
        height: getHeight,
        result: Math.PI * getRadius * getRadius * getHeight
    })
  }


}