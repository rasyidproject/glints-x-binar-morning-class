const area = require('../db/area.json')

module.exports = {

  getCircleArea: (req, res) => {
    res.status(201).json(area.filter(i => i.form == "circle"), null, 2)
  },
  postCircleArea: (req, res) => {
    let getRadius = req.body.parameter.radius
    res.status(201).json({
        form: req.body.form,
        radius: getRadius,
        result: getRadius*Math.PI
    })
  },
  postSquareArea: (req, res) => {
      let getSide = req.body.parameter.side
    res.status(201).json({
        form: req.body.form,
        side: getSide,
        result: getSide*getSide
    })
  },
  postTriangleArea: (req, res) => {
    let getBase = req.body.parameter.base
    let getHeight = req.body.parameter.height
    res.status(201).json({
        form: req.body.form,
        base: getBase,
        height: getHeight,
        result: getBase*getHeight/2
    })
  }
}