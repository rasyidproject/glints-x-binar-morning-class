// Route Level Middleware
const router = require('express').Router()

// Import Controller
const { getCircleArea,postCircleArea,postSquareArea, postTriangleArea } = require('./controller/area')
const { postCubeVolume, postTubeVolume } = require(`./controller/volume`)

router.get('/calculate/circleArea', getCircleArea)
router.post('/calculate/circleArea', postCircleArea)
router.post('/calculate/squareArea', postSquareArea)
router.post('/calculate/triangleArea', postTriangleArea)
router.post('/calculate/postCubeVolume', postCubeVolume)
router.post('/calculate/postTubeVolume', postTubeVolume)
router.get('/info', getInfo)

function getInfo(req, res){
    res.status(201).json({
        "status": true,
        "data": [
            "POST /calculate/circleArea","POST /calculate/squareArea",
            "POST /calculate/triangleArea", "POST /calculate/postCubeVolume",
            "POST /calculate/postTubeVolume"
        ]
    })
}

module.exports = router