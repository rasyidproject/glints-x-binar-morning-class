/*
 * DON'T CHANGE
 * */

const data = [];
let randomNumber = Math.floor(Math.random() * 100);
randomNumber = randomNumber === 0 ? Math.floor(Math.random() * 100) : randomNumber

function createArray() {
  for (let i = 0; i < randomNumber; i++) {
    data.push(createArrayElement())
  }

  // Recursive
  if (data.length == 0) {
    createArray();
  }
}

function createArrayElement() {
  let random = Math.floor(Math.random() * 1000);

  return [null, random][Math.floor(Math.random() * 2)]
}

createArray()

/*
 * Code Here!
 * */

console.log(data);
console.log(clean(data))

function clean(data) {
  // Code here
  let newData = []

  for(i=0;i<data.length;i++){
    if(data[i] != null)
        newData.push(data[i])
  }

  return newData;
}

/*
 * DON'T CHANGE
 * */

if (process.argv.slice(2)[0] == "test") {
  try {
    clean(data).forEach(i => {
      if (i == null) {
        throw new Error("Array still contains null")
      }
    })
  }

  catch(err) {
    console.error(err.message);
  }
}