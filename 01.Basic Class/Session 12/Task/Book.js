const Record = require(`./record`)

class Book extends Record {
    static properties = [
        `title`,`author`,
        `price`,`publisher`
    ]
}

let onepiece = new Book({
    title : `One Piece`,
    author : `Eiichiro Oda`,
    price : 38000,
    publisher : `Weekly Shonen Jump`
})

console.log(onepiece)
onepiece.save()