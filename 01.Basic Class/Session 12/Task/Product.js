const Record = require(`./record`)

class Product extends Record {
    static properties = [
        `name`,`price`,
        `stock`
    ]
}

let shoes = new Product ({
    name : `Nike`,
    price : 2000000,
    stock : 10
})

console.log(shoes)
shoes.save()