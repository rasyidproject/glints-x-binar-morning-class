const Human = require(`../../Session 11/human`)

class Chef extends Human{

    static properties = [...super.properties,`cuisines`,`types`]

    constructor(props){
        super(props)
        this._validate(props)
        this.cuisines = props.cuisines;
        this.types = props.types;
    }

    _validate(props) {
        if (typeof props !== 'object' || Array.isArray(props))
          throw new Error("Constructor needs object to work with");
    
        this.constructor.properties.forEach(i => {
          if (Object.keys(props).includes(i) == false)
            throw new Error(`Some property is missing. ${i} is required`);
        });
    }

    promote(){
        let random = Math.floor(Math.random() * Object.values(this.cuisines).length)
        console.log(`Today we have special menu, ${this.cuisines[random]}. Do you want it?`)
    }

    cook(){
        super.cook()
        console.log(`${this.name} can cook ${this.cuisines}`)
    }

    introduce(){
        super.introduce()
        console.log(`And I'm ${this.types} chef`)
    }
}
let rasyid = new Chef({
    name : `Rasyid`,
    language : [`Javanese`],
    cuisines : [`Indomie`,`Nasi Goreng`,`Telor Rebus`],
    types : [`Indonesian`]
})

console.log(rasyid)
rasyid.introduce()
rasyid.promote()
rasyid.cook()