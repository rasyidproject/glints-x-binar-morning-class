# Session 12

In session 12, I create chef class which inherit some property from Human class in session 11.
To inherit Human class, human.js from is required

```javascript
const Human = require(`../../Session 11/human`)
```
After that, I create chef class and then inherit from Human class
```javascript
class Chef extends Human{
}
```
In this class, there will be 2 new property (<code>cuisines</code> dan <code>types</code>)
```javascript
static properties = [...super.properties,`cuisines`,`types`]
```
To validate the value in class initialization, I create <code>_validate</code> function
```javascript
_validate(props) {
    if (typeof props !== 'object' || Array.isArray(props))
        throw new Error("Constructor needs object to work with");

    this.constructor.properties.forEach(i => {
        if (Object.keys(props).includes(i) == false)
        throw new Error(`Some property is missing. ${i} is required`);
    });
}
```
After this, I create constructor
```javascript
constructor(props){
    super(props)
    this._validate(props)
    this.cuisines = props.cuisines;
    this.types = props.types;
}
```
Finally, I can create 3 behaviour function. For <code>promote()</code> function, there is a variable to store random number which will decide the cuisine to promote
```javascript
promote(){
    let random = Math.floor(Math.random() * Object.values(this.cuisines).length)
    console.log(`Today we have special menu, ${this.cuisines[random]}. Do you want it?`)
}

cook(){
    super.cook()
    console.log(`${this.name} can cook ${this.cuisines}`)
}

introduce(){
    super.introduce()
    console.log(`And I'm ${this.types} chef`)
}
```
Let's test the code. I create a new class object
```javascript
let rasyid = new Chef({
    name : `Rasyid`,
    language : [`Javanese`],
    cuisines : [`Indomie`,`Nasi Goreng`,`Telor Rebus`],
    types : [`Indonesian`]
})
```
And then, call the function
```javascript
rasyid.introduce()
rasyid.promote()
rasyid.cook()
```
Viola, this is the result from terminal. Output from promote function can be different everytime because it's random.
![](/Output/output.jpg)