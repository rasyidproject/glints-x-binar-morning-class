const { Product } = require('../models');
const { Op } = require('sequelize')

module.exports = {
  all(req, res) {
    Product.findAll().then(data => {
      res.status(200).json({
        status: "success",
        data
      })
    })
  },

  find(req, res) {
    Product.findOne({
      where: {
        id: req.params.find
      }
    }).then(data => {
      res.status(200).json({
        status: "success",
        data
      })
    })
  },

  available(req, res) {
    Product.findAll({
      where: {
        stock:{
          [Op.gt]:0 //It means stock>0
        }
      }
    }).then(data => {
      res.status(200).json({
        status: "success",
        data
      })
    })
  },

  insert(req, res) {
    Product.create(req.body).then(data => {
      res.status(201).json({
        status: "success",
        data
      })
    })
  },

  update(req, res) {
    Product.update(
      req.body, {
        where: {
          id: req.params.id
        }
    }).then(data => {
      res.status(200).json({
        message: `data from id ${req.params.id} is updated`
      })
    })
  },

  delete(req, res){
    Product.destroy({
      where: {
        id: req.params.id
      }
    }).then(data => {
      res.status(204).json({
        message: `data from id ${req.params.id} is deleted`
      })
    }).catch(err => {
      res.status(404).json({
        message: `${err.message}`
      })
    })
  }
}