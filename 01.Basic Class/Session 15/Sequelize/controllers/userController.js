const { User } = require('../models');

const bcrypt = require('bcryptjs')


module.exports = {
  
  register:(req,res) => {
    let salt = bcrypt.genSaltSync(10)
    let hash = bcrypt.hashSync(req.body.password,salt)
    let getVerified = (req.body.verified == null) ? false : req.body.verified
    let getEmail = (req.body.email).toLowerCase()
    console.log(`${req.body.email} => ${getEmail}`)
    User.create({
      email: getEmail,
      encrypted_password: hash,
      verified: getVerified
    }).then(data => {
      res.status(201).json({
        status: "success",
        data
      })
    }).catch(err => {
      res.status(422).json({
        status: `fail`,
        errors: [err.message]
      })
    })
  },

  getUser: (req, res) => {
    User.findAll().then(data => {
      res.status(200).json({
        status: "success",
        data
      })
    }).catch(err => {
      res.status(422).json({
        status: `fail`,
        errors: [err.message]
      })
    })
  },

  login: (req, res) => {
    let getEmail = (req.body.email).toLowerCase()
    User.findOne({
      where: {
        email: getEmail
      }
    }).then(user =>{
      if(!user)
        return res.status(401).json({
          status: `fail`,
          errors: [`email doesn't exist`]
        })

      let hash = user.encrypted_password
      if(bcrypt.compareSync(req.body.password, hash)){
        res.status(200).json({
          status: `login successful`,
          data : {
            user: user.entity
          }
        })
      }else{
        res.status(401).json({
          status: `fail`,
          errors: [`wrong password`]
        })
      }
    }).catch(err => {
      res.status(422).json({
        status: `fail`,
        errors: [err.message]
      })
    })
  }  
}