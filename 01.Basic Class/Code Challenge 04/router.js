const router = require('express').Router()

const roleController = require(`./controllers/roleController`)
const userController = require(`./controllers/userController`)
const postController = require(`./controllers/postController`)

const success = require(`./middlewares/success`)
const authorizer = require(`./middlewares/authorizer`)
const check = require(`./middlewares/checkOwnership`)
const checkRole = require(`./middlewares/checkRole`)

router.post(`/roles`, roleController.create, success)
router.get(`/roles`, roleController.all, success)

router.post(`/users`, userController.register, success)
router.post(`/users/login`, userController.login, success)
router.get(`/users`, userController.all, success)
router.get(`/users/:id`, authorizer, check(`users`,``), userController.me, success)
router.put(`/users/:id`, authorizer, check(`users`,``), userController.update, success)
router.delete(`/users/:id`, authorizer, check(`users`,``), userController.delete, success)

router.post(`/posts`, authorizer, postController.newPost, success)
router.get(`/posts/:id`, authorizer, check(`posts`,``), checkRole,postController.me, success)
router.get(`/posts-debug`, postController.allDebug,success)
router.get(`/posts-all`, authorizer, checkRole,postController.allWithPrivilage,success)
router.put(`/posts/:id`,authorizer, check(`posts`,`update`),postController.update,success)
router.put(`/posts/admin/:id`,authorizer, check(`posts`,``),checkRole,postController.postVerify,success)
router.delete(`/posts/:id`,authorizer, check(`posts`),postController.delete,success)

module.exports = router