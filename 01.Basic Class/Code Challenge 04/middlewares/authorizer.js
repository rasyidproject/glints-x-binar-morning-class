const jwt = require('jsonwebtoken');
const {users} = require('../models');

module.exports = async (req, res, next) => {
  try {
    let token = req.headers.authorization;
    let payload = jwt.verify(token, process.env.TOKEN);
    req.user = await users.findByPk(payload.id);
    next(); 
  }

  catch {
    res.status(401);
    next(new Error("Invalid Token"));
  }
}