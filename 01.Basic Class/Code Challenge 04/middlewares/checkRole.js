const { posts,role } = require('../models');

module.exports = async(req, res, next) => {
    try {
        let currentRole = await role.findOne({
            where : {
                id : req.user.id_role
            }
        })
        console.log(`You are ${currentRole.name}`)
        req.privilage = currentRole.post_moderation
        next()
    } catch (error) {
        next(error)
    }
}