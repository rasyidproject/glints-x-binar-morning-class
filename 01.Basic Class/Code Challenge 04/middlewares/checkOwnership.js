const models = require('../models');

module.exports = (modelName,action) => {
  const Model = models[modelName];

  return async function(req, res, next) {
    try {
      let instance = await Model.findByPk(req.params.id)
      let errorForbid = new Error(`This is not your ${modelName}`)
      if(modelName == `users`){
        if (instance.id !== req.user.id) {
          res.status(403);
          return next(errorForbid)
        }
      }else if(modelName == `posts`){
        let checkRole = await models[`role`].findByPk(req.user.id_role)
        
        if (instance.user_id !== req.user.id) {
          if(checkRole.post_moderation == false || action == `update`){
            res.status(403);
            return next(errorForbid)
          }
        }
      }
      next();
    } catch (error) {
      next(error)
    }
  }
}