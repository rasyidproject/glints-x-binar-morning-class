const { posts, users, role } = require('../models')

module.exports = {
    newPost: async (req, res, next) => {
        try {
            const data = await posts.create({
                title: req.body.title,
                body: req.body.body,
                user_id: req.user.id
            })
            res.status(200)
            res.data = data
            next()
        } catch (error) {
            next(error)
        }
    },
    allDebug: async (req, res, next) => {
        try {
            const data = await posts.findAll({
                include: [users]
            })
            res.status(200)
            res.data = data
            next()
        } catch (error) {
            next(error)
        }
    },
    allWithPrivilage: async (req, res, next) => {
        console.log(req.privilage)
        try {
            let data
            if (req.privilage) {
                data = await posts.findAll()
            }else {
                data = await posts.findAll({
                    where: {
                        visibility: true
                    }
                })
            }
            res.status(200)
            res.data = data
            next()
        } catch (error) {
            next(error)
        }
    },
    postVerify: async (req, res, next) => {
        console.log(req.privilage)
        try {
            let data
            if(req.privilage){
                data = await posts.update({
                    visibility: true
                }, {
                        where: {
                            id: req.params.id
                        }
                    }
                )
            }

            // If data not found, send error message
            if(data[0] === 0){
                res.status(404)
                return next(new Error(`There are no post with id ${req.params.id}`))
            }

            res.status(200)
            res.data = `post from id ${req.params.id} is verified`
            next()
        } catch (error) {
            res.status(422)
            next(error)
        }
    },
    me: async (req, res, next) => {
        try {
            console.log(req.privilage)
            const data = await posts.findAll({
                where : {
                    user_id : req.params.id
                }
            })
            res.status(200)
            res.data = data
            next()
        } catch (error) {
            next(error)
        }
    },
    update: async (req, res, next) => {

        // Prevent update visibility through this endpoint
        if(req.body.visibility != null){
            return next(new Error(`Action denied`))
        }

        try {
            const data = await posts.update(
                req.body, {
                    where: {
                        id: req.params.id
                    }
                }
            )

            // If data not found, send error message
            if(data[0] === 0){
                res.status(404)
                return next(new Error(`There are no post with id ${req.params.id}`))
            }

            res.status(200)
            res.data = `post from id ${req.params.id} is updated`
            next()
        } catch (error) {
            res.status(422)
            next(error)
        }
    },
    delete: async (req, res, next) => {
        try {
            const data = await posts.destroy({
                where : {
                    id : req.params.id
                }
            })

            // If data not found, send error message
            if(data === 0){
                res.status(404)
                return next(new Error(`There are no data with id ${req.params.id}`))
            }

            res.status(204)
            req.data = `data from id ${req.params.id} is deleted`
            next()
        } catch (error) {
            res.status(404)
            next(error)
        }
    }
}