const { role } = require(`../models`)

module.exports = {
    create : async (req, res, next) => {
        try {
            const data = await role.create({
                name: req.body.name,
                post_moderation: req.body.post_moderation
            })

            res.data = data
            next()
        } catch (error) {
            res.status(422)
            next(error)
        }
    },
    all : async (req, res, next) => {
        try {
            const data = await role.findAll()
            res.data = data
            next()
        } catch (error) {
            res.status(422)
            next(error)
        }
    }
}