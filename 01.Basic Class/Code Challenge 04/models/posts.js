'use strict';
module.exports = (sequelize, DataTypes) => {
  const posts = sequelize.define('posts', {
    title: DataTypes.STRING,
    body: DataTypes.TEXT,
    visibility: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    user_id: DataTypes.INTEGER
  }, {});
  posts.associate = function(models) {
    posts.hasOne(models.users, {
      foreignKey: 'id'
    })
  };
  return posts;
};