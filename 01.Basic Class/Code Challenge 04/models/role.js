'use strict';
module.exports = (sequelize, DataTypes) => {
  const role = sequelize.define('role', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isLowercase: true,
        notEmpty: true
      }
    },
    post_moderation: {
      type: DataTypes.BOOLEAN,
      validate: {
        notEmpty: true
      },
      allowNull: false,
      defaultValue: false
    }
  }, {});
  role.associate = function(models) {
    // associations can be defined here
  };
  return role;
};