const sleep = ms => {
    return new Promise(
        resolve => setTimeout(resolve, ms)
    )
}

let fridge = [
    "Apple", "Wortel", "Milk",
    "Orange", "Leech", "Cabbage"
]

//Randomly set the egg availability
if (Math.round(Math.random() * 1) == 1){
    fridge.push("Egg");
}

function checkTheEggAvailability() {
    return new Promise(async resolve => {
        console.clear();
        console.log("Checking the egg availability...")
        await sleep(2000)
        
        // Check the egg availability
        for (let i=0; i<fridge.length;i++){
            if(fridge[i]=="Egg"){
                console.log("There's an egg!")
                await sleep(1000)
                resolve(true)
            }
        }

        // If no egg found
        console.log("There's no egg!")
        await sleep(1000)
        resolve(false)
    })
}

// Function to check wallet
async function checkWallet(thereIsEgg){
    return new Promise(async resolve =>{
        if(thereIsEgg){
            console.clear()
            console.log("Checking the wallet...")
            await sleep(2000)
    
            let doWeHaveMoney = Math.round(Math.random() * 1)

            if(doWeHaveMoney==1){
                console.log("Yeay, we have enough money")
                await sleep(1000)
                resolve(true)
            }

            console.log("We dont have money. Let's eat what we already have")
            await sleep(1000)
            resolve(false)
        }
    })
    

    await sleep(4000)
}

// Function to handle whether we have to go to market or not
async function buyAnEgg(shouldWe){
    if(shouldWe){
        console.clear()
        console.log("Heading to the market...")
        await sleep(4000)
        console.log("Buying an egg")
        await sleep(1000)
    }
}

// Function to prepare the fry
async function prepareTheFry() {
    console.clear();
    console.log("Pouring the oil...")
    await sleep(1000)

    console.clear()
    console.log("Lit up the stove...")
    await sleep(1000)
}

// Fry the egg
function fryTheEgg(){
    return new Promise(resolve => {
        console.clear()
        console.log("Cook the egg...")
        sleep(4000).then(() => {
            console.clear()
            resolve("Egg is ready to eat")
        })
    })
}

async function cookAnEgg(){
    await prepareTheFry()

    console.log(
        await fryTheEgg()
    )
}

async function beforeCooking(){
    // Check if the egg is available
    let checkEgg = await checkTheEggAvailability()

    if(checkEgg){
        cookAnEgg()
    }else {
        // Check wallet
        let checkMoney = await checkWallet(!checkEgg)
        // If we dont have money, we dont eat egg
        if(checkMoney){
            cookAnEgg()
        }
    }
}

beforeCooking()